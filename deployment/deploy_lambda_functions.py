import os
import subprocess
import awsclients
from pathlib import Path
from awsclients import Helper


class DeployLambdaFunctions(object):
    def __init__(self):
        self.parent_dir = os.pardir
        self.current_working_dir = os.getcwd()
        self.helpers = awsclients.Helper()
        self.client = awsclients.LambdaClient()
        self.tmp_directory = "tmp"
        self.DEVNULL = open(os.devnull, 'w')
        self.logger = Helper.create_logger(__name__)

    def get_configuration(self, configuration_type=None):
        configuration_path = os.path.abspath("{}/{}/".format(self.parent_dir, configuration_type))
        print("{}/{}/".format(Path(configuration_path).parent, self.tmp_directory))
        if os.path.exists("{}/{}/".format(Path(configuration_path).parent, self.tmp_directory)):
            self.helpers.remove_directory("{}/{}".format(Path(configuration_path).parent, self.tmp_directory))

        for each_json_config_file in self.helpers.walk_through_files(configuration_path):
            configuration = self.client.get_configuration(each_json_config_file)

            tmp_lambda_package_location = "{}/{}/{}/".format(Path(configuration_path).parent, "tmp",
                                                             configuration["function_name"])
            directories_to_copy = ["{}/".format(Path(each_json_config_file).parent)]
            for library in configuration['libraries']:
                directories_to_copy.append("{}/{}/{}/".format(Path(configuration_path).parent, "libraries", library))
            self.helpers.make_directories(tmp_lambda_package_location, exist_ok=True)
            for each_source_dir in directories_to_copy:
                self.helpers.copy_files_and_directories(each_source_dir, tmp_lambda_package_location)
            self.helpers.install_dependencies(configuration['python_packages'],tmp_lambda_package_location)
            self.zip_package(configuration['function_name'], tmp_lambda_package_location)
            lambda_configuration = configuration['configuration']
            lambda_function_s3_bucket = configuration['configuration']['Code']['S3Bucket']
            lambda_function_s3_key = configuration['configuration']['Code']['S3Key']
            self.upload_lambda_function_package_to_s3(tmp_lambda_package_location,
                                                      lambda_function_s3_bucket,
                                                      lambda_function_s3_key)
            self.client.create_function(lambda_configuration)
            if os.path.exists("{}/{}/".format(Path(configuration_path).parent, self.tmp_directory)):
                self.helpers.remove_directory("{}/{}".format(Path(configuration_path).parent, self.tmp_directory))

    def zip_package(self, zip_file_name, path_to_be_zipped):
        self.helpers.change_directory(path_to_be_zipped)
        subprocess.call(['zip', '-r9', '{}.zip'.format(zip_file_name), '.'], stdout=self.DEVNULL)

    def upload_lambda_function_package_to_s3(self, location_of_zip_file, s3_bucket, s3_key):
        self.helpers.change_directory(location_of_zip_file)
        s3_client = awsclients.S3Client()
        s3_client.upload_file(s3_key, s3_bucket, s3_key)


if __name__ == '__main__':
    dlf = DeployLambdaFunctions()
    dlf.get_configuration("lambdas")
