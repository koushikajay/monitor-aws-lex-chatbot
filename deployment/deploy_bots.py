import os.path
# Deploy Bot
import awsclients
from awsclients import Helper


class DeployBots(object):
    def __init__(self):
        self.client = awsclients.LexClient()
        self.parent_dir = os.pardir
        self.helpers = Helper()
        self.configurations = ["bots", "intents", "slots"]
        self.logger = Helper.create_logger(__name__)

    def get_configuration(self, configuration_type=None):
        configurations = []
        resources = {}
        configuration_path = os.path.abspath("{}/{}/".format(self.parent_dir, configuration_type))
        for each_json_config_file in self.helpers.walk_through_files(configuration_path):
            # configuration = self.client.__getattribute__("get_{}_configuration".format(configuration_type[:-1]))(
            #     each_json_config_file)
            configuration = self.client.get_configuration(each_json_config_file)
            configurations.append(configuration)
        resources[configuration_type] = configurations
        return resources

    def deploy_resources(self):

        resources = ["slots", "intents"]
        self.logger.info("Deploying Resources:{}".format(resources))
        for each_resource in resources:
            self.logger.info("Obtaining the Configuration of the Resource {}".format(each_resource))
            all_resources = self.get_configuration(each_resource)
            for each_lex_resource_configuration in all_resources[each_resource]:
                checksum = self.client.get_checksum(each_lex_resource_configuration['name'], each_resource[:-1])
                self.client.__getattribute__("put_{}".format(each_resource[:-1]))(each_lex_resource_configuration,
                                                                                  checksum)

    def deploy_bots(self):
        all_bot_resources = self.get_configuration("bots")
        for each_bot_configuration in all_bot_resources["bots"]:
            checksum = self.client.get_checksum(each_bot_configuration['name'], "bot")
            self.client.put_bot(each_bot_configuration, checksum)

    def main(self):
        self.logger.info("Entering into the Main Function")
        self.deploy_resources()
        self.deploy_bots()


if __name__ == '__main__':
    db = DeployBots()
    db.main()
