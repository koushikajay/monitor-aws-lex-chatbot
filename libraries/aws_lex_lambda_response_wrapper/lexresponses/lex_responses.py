

# --- Helpers that build all of the responses ---
class LexResponse(object):
    def __init__(self, **kwargs):
        pass

    # --- Helpers that build all of the responses ---

    @staticmethod
    def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit):
        print("slots inside elicitslot:{}".format(slots))
        print("slot to elicit inside elicitslot:{}".format(slot_to_elicit))
        return {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'intentName': intent_name,
                'slots': slots,
                'slotToElicit': slot_to_elicit

            }
        }

    @staticmethod
    def confirm_intent(session_attributes, intent_name, slots, message):
        return {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ConfirmIntent',
                'intentName': intent_name,
                'slots': slots,
                'message': message
            }
        }

    @staticmethod
    def close(session_attributes, fulfillment_state, message):
        response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': fulfillment_state,
                'message': message
            }
        }

        return response

    @staticmethod
    def delegate(session_attributes, slots):
        return {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Delegate',
                'slots': slots
            }
        }
