from setuptools import setup, find_packages

setup(
    name='awsclients',
    version='0.1',
    description="AWS Boto3 Wrapper",
    author="Subram",
    author_email="subram.mahalingam@gmail.com",
    license="Copyright Subram",
    packages=find_packages('.'),
    install_requires=['boto3','elasticsearch'],
    include_package_data=True,
    zip_safe=False,
    data_files=[]
)
