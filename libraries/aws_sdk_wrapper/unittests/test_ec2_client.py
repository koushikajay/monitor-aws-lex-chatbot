import unittest

from libraries.aws_sdk_wrapper import EC2Client


class TestEC2Client(unittest.TestCase):
    def test_start_instances_invalid_instance_id(self):
        ec2_client = EC2Client()
        instance_id = ["i-0b26f3ff787b77081"]
        # self.assertEqual(ec2_client.start_instances(instance_id),True,"Should be True ")
        self.assertEqual(ec2_client.start_instances(instance_ids=instance_id), True, "Should start instance")


if __name__ == '__main__':
    unittest.main()
