from botocore.exceptions import ClientError
from .aws_client import *
import boto3


class LambdaClient(AWSClient):
    def __init__(self):
        AWSClient.__init__(self)
        self.aws_service = 'lambda'
        self.helper = Helper()
        self.logger = Helper.create_logger(__name__)

    def initiate_client(self):
        __lambda_client = boto3.client(self.aws_service)
        return __lambda_client

    def add_permission(self, lambda_function_name, statement_id, action, principal):
        try:
            client = self.initiate_client()
            response = client.add_permission(
                FunctionName=lambda_function_name,
                StatementId=statement_id,
                Action=action,
                Principal=principal
            )
            self.logger.info("Adding Policy to Lambda Function {}".format(lambda_function_name))
            self.logger.debug("Response from Lambda Permission:{} ".format(response))
        except Exception as e:
            raise e

    def remove_permission(self, lambda_function_name, statement_id):
        try:
            client = self.initiate_client()
            response = client.remove_permission(
                FunctionName=lambda_function_name,
                StatementId=statement_id,

            )
            self.logger.debug("Response from Removing Lambda Permission:{} ".format(response))
        except Exception as e:
            raise e

            # def get_configuration(self, configuration_path):
            #     return self.helper.read_json_file(configuration_path)

    def update_function_configuration(self, function_configuration):
        try:
            self.logger.info("Updating Configuration........")
            del function_configuration['Publish']
            del function_configuration['Code']
            del function_configuration['Tags']
            client = self.initiate_client()
            response = client.update_function_configuration(**function_configuration)
            self.logger.info("Configuration Updated :{}".format(response))
        except Exception as e:
            raise e

    def update_function_code(self, function_configuration):
        try:
            self.logger.info("Updating Function Code........")
            client = self.initiate_client()
            response = client.update_function_code(
                FunctionName=function_configuration['FunctionName'],
                S3Bucket=function_configuration['Code']['S3Bucket'],
                S3Key=function_configuration['Code']['S3Key'],

            )
            self.logger.info("Configuration Code Updated :{}".format(response))
        except Exception as e:
            raise e

    def create_function(self, function_configuration):
        try:
            client = self.initiate_client()
            response = client.create_function(**function_configuration)
            self.logger.info("Lambda Function Created :{}".format(response))
        except ClientError as e:
            if (e.response['Error']['Code']) == 'ResourceConflictException':
                self.logger.info("ResourceAlready Exist")
                self.update_function_code(function_configuration)
                self.update_function_configuration(function_configuration)

    def get_policy(self, lambda_function_name):
        policy = []
        try:
            client = self.initiate_client()
            response = client.get_policy(
                FunctionName=lambda_function_name
            )
            policy.append(response['Policy'])
            return policy
        except ClientError as e:
            if (e.response['Error']['Code']) == 'ResourceNotFoundException':
                return policy
