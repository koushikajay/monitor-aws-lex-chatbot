from .lambda_client import *
from .aws_client import AWSClient
import boto3
from .helper import *


class LexClient(AWSClient):
    def __init__(self):
        AWSClient.__init__(self)
        self.aws_service = 'lex-models'
        self.helper = Helper()
        self.logger = Helper.create_logger(__name__)

    def initiate_client(self):
        __lex_client = boto3.client(self.aws_service)
        return __lex_client

    def put_intent(self, intent_configuration, checksum):
        try:
            uri = ''
            client = self.initiate_client()
            if 'dialogCodeHook' in intent_configuration:
                uri = intent_configuration['dialogCodeHook']['uri']
            if 'fulfillmentActivity' in intent_configuration:
                uri = intent_configuration['fulfillmentActivity']['codeHook']['uri']
            # add lambda permission
            _lambda_client = LambdaClient()
            policies = _lambda_client.get_policy(uri)
            if len(policies) > 0:
                _lambda_client.remove_permission(
                    uri,
                    "Intent{}Permission".format(intent_configuration['name'])
                )
            _lambda_client.add_permission(uri,
                                          "Intent{}Permission".format(intent_configuration['name']),
                                          "lambda:invokeFunction",
                                          "lex.amazonaws.com"
                                          )
            intent_configuration['checksum'] = checksum
            response = client.put_intent(**intent_configuration)
            self.logger.info("Deployed Intent {} with version {}".format(response['name'], response['version']))
        except ClientError as e:
            raise e

    def get_intents(self, max_results=10):
        try:
            client = self.initiate_client()
            current_intents = []
            response = client.get_intents(maxResults=max_results)
            for intent in response['intents']:
                current_intents.append(intent['name'])
            return current_intents
        except ClientError as e:
            raise e

    def put_slot(self, slot_type_configuration, checksum):
        try:
            client = self.initiate_client()
            slot_type_configuration['checksum'] = checksum
            from ast import literal_eval
            slot_type_configuration['createVersion'] = literal_eval(slot_type_configuration['createVersion'])

            response = client.put_slot_type(**slot_type_configuration)
            self.logger.info("Deployed Slot Type {} with version {}".format(response['name'], response['version']))
        except ClientError as e:
            raise e

    def put_bot(self, bot_configuration, checksum):
        try:
            client = self.initiate_client()
            bot_configuration['checksum'] = checksum
            response = client.put_bot(**bot_configuration)
            self.logger.info("Deployed Bot  {} with version {}".format(response['name'], response['version']))
        except ClientError as e:
            raise e

    def get_configuration(self, configuration_path):
        return self.helper.read_json_file(configuration_path)

    def delete_slot_type(self, slot_type_name):
        try:
            client = self.initiate_client()
            response = client.delete_slot_type(name=slot_type_name)
            self.logger.debug("Slot Type Delete Response:{} ".format(response))
            self.logger.info("Deleted Slot Type {}".format(slot_type_name))
        except ClientError as e:
            raise e

    def get_slot_type(self, slot_type_name, version='$LATEST'):
        try:
            client = self.initiate_client()
            response = client.get_slot_type(name=slot_type_name, version=version)
            self.logger.info("Getting Slot Type {}".format(slot_type_name))
            return response
        except ClientError as e:
            raise e

    def get_intent_type(self, intent_name, version='$LATEST'):
        try:
            client = self.initiate_client()
            response = client.get_intent(name=intent_name, version=version)
            self.logger.info("Getting Intent Type {}".format(intent_name))
            return response
        except ClientError as e:
            raise e

    def get_bot_type(self, bot_name, version='$LATEST'):
        try:
            client = self.initiate_client()
            response = client.get_bot(name=bot_name, versionOrAlias=version)
            self.logger.info("Getting Bot Type {}".format(bot_name))
            return response
        except ClientError as e:
            raise e

    @staticmethod
    def get_checksum(name, resource_type):
        lc = LexClient()
        metadata = lc.__getattribute__("get_{}_type".format(resource_type))(name)
        return metadata['checksum']
