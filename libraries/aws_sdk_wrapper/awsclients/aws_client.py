from .helper import Helper


class AWSClient(object):
    def __init__(self):
        self.default_region = 'us-east-1'
        self.helper = Helper()

    def initiate_client(self):
        pass

    def get_configuration(self, configuration_path):
        return self.helper.read_json_file(configuration_path)
