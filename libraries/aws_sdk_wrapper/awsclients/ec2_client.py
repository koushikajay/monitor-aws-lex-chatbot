import boto3
from botocore.exceptions import ClientError
from .aws_client import AWSClient
from .helper import Helper


class EC2Client(AWSClient):
    def __init__(self):
        AWSClient.__init__(self)
        self.aws_service = 'ec2'
        self.logger = Helper.create_logger(__name__)

    def initiate_client(self):
        __ec2_client = boto3.client(self.aws_service)
        return __ec2_client

    def list_instances(self, max_results=100, instance_filters=None):
        if max_results >= 1000:
            raise Exception("Maximum Results can be Less than or Equal to 1000")

        try:
            client = self.initiate_client()
            if instance_filters is None:
                filters = []
            else:
                filters = [instance_filters]
            response = client.describe_instances(
                Filters=filters,
                MaxResults=max_results

            )
            instance_metadata = {}
            instances = []
            self.logger.info(response)
            for each_reservation in response['Reservations']:
                for each_instance in each_reservation['Instances']:
                    instance_metadata['id'] = each_instance['InstanceId']
                    instance_metadata['type'] = each_instance['InstanceType']
                    instance_metadata['private_ip_address'] = each_instance['PrivateIpAddress']
                    instance_metadata['state'] = each_instance['State']['Name']
                    instance_metadata['tags'] = each_instance['Tags']
                    instances.append(instance_metadata)
            self.logger.info(instances)
            return instances
        except ClientError as e:
            raise e

    def get_instance_id(self, instance_names):
        ec2_instance_ids = []
        filters = {
            'Name': 'tag:Name',
            'Values': instance_names
        }
        ec2_instances = self.list_instances(instance_filters=filters)
        try:
            for ec2_instance_metadata in ec2_instances:
                for tags in ec2_instance_metadata['tags']:
                    if tags['Key'] == 'Name':
                        if tags['Value'] in instance_names:
                            ec2_instance_ids.append(ec2_instance_metadata['id'])
            return ec2_instance_ids

        except Exception as e:
            raise e

    def start_instances(self, instance_ids):

        try:
            client = self.initiate_client()
            response = client.start_instances(
                InstanceIds=instance_ids

            )
            self.logger.info("Starting EC2 Instance(s) Metadata:{} ".format(response))
            instances_started = True
            return instances_started
        except Exception as e:
            raise e

    def stop_instances(self, instance_ids):
        try:
            client = self.initiate_client()
            response = client.stop_instances(
                InstanceIds=instance_ids

            )
            self.logger.info("Stopping EC2 Instance(s) Metadata:{} ".format(response))
            instances_stopped = True
            return instances_stopped
        except Exception as e:
            raise e

    def terminate_instances(self, instance_ids):
        try:
            client = self.initiate_client()
            response = client.stop_instances(
                InstanceIds=instance_ids

            )
            self.logger.info("Terminating EC2 Instance(s) Metadata:{} ".format(response))
            instances_terminated = True
            return instances_terminated
        except Exception as e:
            raise e

    def filter_instance_by_tag(self):
        pass
