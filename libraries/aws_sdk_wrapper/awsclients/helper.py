import json
import subprocess
from distutils.dir_util import copy_tree
from distutils.errors import DistutilsFileError
import os
import shutil
import logging
import logging.config
import yaml
import pkg_resources
import six

configured = False
CONFIG_FILE = "logging.conf"
unicode = six.text_type
long = six.integer_types
DEVNULL = open(os.devnull, 'w')


@six.add_metaclass(type)
class Helper(object):
    @staticmethod
    def walk_through_files(path, file_extension='.json'):
        for (directory_path, directory_names, filenames) in os.walk(path):
            for filename in filenames:
                if filename.endswith(file_extension):
                    yield os.path.join(directory_path, filename)

    @staticmethod
    def read_json_file(json_file_path=None):
        with open(json_file_path) as json_file:
            data = json.load(json_file)
            return data

    @staticmethod
    def make_directories(path, exist_ok=True):
        try:
            os.makedirs(path, exist_ok=exist_ok)
        except OSError as e:
            helper_logger.info("Creation of the directory %s failed" % path)
            raise e
        else:
            helper_logger.info("Successfully created the directory %s " % path)

    @staticmethod
    def copy_files_and_directories(src, dst):
        try:
            copy_tree(src, dst)
        except DistutilsFileError as e:
            raise e

    @staticmethod
    def remove_directory(path):
        try:
            shutil.rmtree(path)
        except OSError as e:
            raise e

    @staticmethod
    def change_directory(path):
        try:
            os.chdir(path)
        except OSError as e:
            raise e

    @staticmethod
    def install_dependencies(packages_to_be_installed, packages_path):
        for package_name in packages_to_be_installed:
            subprocess.call(
                ['/Users/smahalingam/venv/Python3.6/bin/pip', 'install', '--no-deps', '--force-reinstall', package_name,
                 '-t',
                 packages_path])

    @staticmethod
    def create_logger(logger_name, logging_config_dict=None):
        """Create a Python logging object with global configuration

        Parameters
        ----------
            logger_name : string
                name of the logger (usually the name of the class using it)

            logging_config_dict : dict
                dictionary object with pythong logging config

        Returns
        -------
        Object
            logging.logger object
        """
        global configured

        # Set up logging
        if logging_config_dict is None:
            if not configured:
                conf_file_data = pkg_resources.resource_string(__name__, CONFIG_FILE)
                config = yaml.load(conf_file_data)
                config.setdefault('version', 1)
                logging.config.dictConfig(config)
                configured = True
            logger = logging.getLogger(logger_name)
            logger.debug(
                "created logger {0} using default config inside the egg : {1}".format(logger_name, CONFIG_FILE))
        else:
            logging.config.dictConfig(logging_config_dict)
            configured = True
            logger = logging.getLogger(logger_name)
            logger.debug("created logger {0} using supplied external config".format(logger_name))
        return logger


h = Helper()
helper_logger = h.create_logger(__file__)
