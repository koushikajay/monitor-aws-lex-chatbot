from .aws_client import AWSClient
from .helper import Helper
import boto3


class S3Client(AWSClient):
    def __init__(self):
        AWSClient.__init__(self)
        self.aws_service = 's3'
        self.logger = Helper.create_logger(__name__)

    def initiate_client(self):
        self.logger.info("Initiating {} client".format(self.aws_service))
        s3_client = boto3.client(self.aws_service)
        return s3_client

    def upload_file(self, filename, bucket, key):
        s3_resource = boto3.resource(self.aws_service)
        s3_resource.meta.client.upload_file(filename, bucket, key)
