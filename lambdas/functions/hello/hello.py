# Lambda Function for Handling Instances Intent
import logging
from lexresponses import LexResponse

lex_response = LexResponse()
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


# --- Intents ---

def dispatch_intent(intent_request):
    """
    Called when the user specifies an intent for this bot.
    """

    logger.debug(
        'dispatch userId={}, intentName={}'.format(intent_request['userId'], intent_request['currentIntent']['name']))
    logger.debug("Current Intent Request: {}".format(intent_request))
    session_attributes = {}
    fulfillment_state = 'Fulfilled'
    response_string = "How May I Help you ??"
    message = {'contentType': 'PlainText', 'content': response_string}
    return lex_response.close(session_attributes, fulfillment_state=fulfillment_state, message=message)


# Main Handler
def lambda_handler(event, context):
    """
   Route the incoming request based on intent.
   The JSON body of the request is provided in the event slot.
   """

    logger.debug('event.bot.name={}'.format(event['bot']['name']))

    return dispatch_intent(event)
