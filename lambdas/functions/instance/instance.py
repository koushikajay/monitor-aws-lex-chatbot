# Lambda Function for Handling Instances Intent
import logging

from awsclients import EC2Client
import lexresponses

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
lex_response = lexresponses.LexResponse()


# Main Method to handle the Intent Call
def monitor_instances(intent_request):
    """
    Performs Monitoring of EC2 Instances in your account
    :param intent_request: Lex Event Metadata
    :return: Lex Lambda Response based on user's input
    """
    slots = intent_request['currentIntent']['slots']
    intent_name = intent_request['currentIntent']['name']
    logger.info("Slots for Intent {}:{}".format(intent_name, slots))
    instance_operation = slots['operations']
    session_attributes = intent_request['sessionAttributes']
    logger.info("InstanceOperation:{}".format(instance_operation))
    logger.info("SessionAttributes:{}".format(session_attributes))
    input_transcript = intent_request['inputTranscript']
    if instance_operation is None:
        return lex_response.elicit_slot(session_attributes, intent_name, slots, "operations")
    if intent_request['invocationSource'] == 'DialogCodeHook':

        if input_transcript in ["start", "stop", "terminate", "create"]:
            # validate input slots
            message = {
                "contentType": "PlainText",
                "content": "Please provide the Instance Name to {}".format(instance_operation)
            }
            return lex_response.confirm_intent(session_attributes, intent_name, slots, message)
        if instance_operation == 'start':
            logger.info("Intent Name:{}".format(intent_name))
            logger.info("Operation being invoked {}".format(instance_operation.lower()))
            result = start_instances(input_transcript)
            return result
        if instance_operation == 'list':
            logger.info("Intent Name:{}".format(intent_name))
            logger.info("Operation being invoked {}".format(instance_operation.lower()))
            result = list_instances()
            return result
        if instance_operation == 'stop':
            logger.info("Intent Name:{}".format(intent_name))
            logger.info("Operation being invoked {}".format(instance_operation.lower()))
            result = stop_instances(input_transcript)
            return result
        if instance_operation == 'terminate':
            logger.info("Intent Name:{}".format(intent_name))
            logger.info("Operation being invoked {}".format(instance_operation.lower()))
            result = terminate_instances(input_transcript)
            return result


def list_instances():
    ec2client = EC2Client()
    list_of_ec2_instances = ec2client.list_instances()
    contents = []
    for each_instance in list_of_ec2_instances:
        for each_instance_tag in each_instance['tags']:
            instance_name = each_instance_tag['Value']
            contents.append(instance_name)
    session_attributes = {}
    fulfillment_state = 'Fulfilled'
    message = {
        "contentType": "PlainText",
        "content": ','.join(contents)
    }
    lex_response_output = lex_response.close(session_attributes=session_attributes,
                                             fulfillment_state=fulfillment_state,
                                             message=message)
    return lex_response_output


def start_instances(ec2_instance_names):
    ec2client = EC2Client()
    ec2_instance_names = ec2_instance_names.split(',')
    if isinstance(ec2_instance_names, str):
        ec2_instance_names = [ec2_instance_names]
    ec2_instance_ids = ec2client.get_instance_id(ec2_instance_names)
    logger.info(ec2_instance_names)
    logger.info(ec2_instance_ids)
    started = ec2client.start_instances(ec2_instance_ids)
    session_attributes = {}
    fulfillment_state = 'Failed'
    message = {
        "contentType": "PlainText",
        "content": ' {} Instance(s) Started'.format(ec2_instance_names)
    }
    if started:
        fulfillment_state = 'Fulfilled'

    lex_response_output = lex_response.close(session_attributes=session_attributes,
                                             fulfillment_state=fulfillment_state,
                                             message=message)
    return lex_response_output


def stop_instances(ec2_instance_names):
    ec2client = EC2Client()
    ec2_instance_names = ec2_instance_names.split(',')
    if isinstance(ec2_instance_names, str):
        ec2_instance_names = [ec2_instance_names]
    ec2_instance_ids = ec2client.get_instance_id(ec2_instance_names)
    logger.info(ec2_instance_names)
    logger.info(ec2_instance_ids)
    stopped = ec2client.stop_instances(ec2_instance_ids)
    session_attributes = {}
    fulfillment_state = 'Failed'
    message = {
        "contentType": "PlainText",
        "content": ' {} Instance(s) Stopped'.format(ec2_instance_names)
    }
    if stopped:
        fulfillment_state = 'Fulfilled'

    lex_response_output = lex_response.close(session_attributes=session_attributes,
                                             fulfillment_state=fulfillment_state,
                                             message=message)
    return lex_response_output


def terminate_instances(ec2_instance_names):
    ec2client = EC2Client()
    ec2_instance_names = ec2_instance_names.split(',')
    if isinstance(ec2_instance_names, str):
        ec2_instance_names = [ec2_instance_names]
    ec2_instance_ids = ec2client.get_instance_id(ec2_instance_names)
    logger.info(ec2_instance_names)
    logger.info(ec2_instance_ids)
    terminated = ec2client.terminate_instances(ec2_instance_ids)
    session_attributes = {}
    fulfillment_state = 'Failed'
    message = {
        "contentType": "PlainText",
        "content": ' {} Instance(s) Terminated'.format(ec2_instance_names)
    }
    if terminated:
        fulfillment_state = 'Fulfilled'

    lex_response_output = lex_response.close(session_attributes=session_attributes,
                                             fulfillment_state=fulfillment_state,
                                             message=message)
    return lex_response_output


def validate_monitor_instance(slots):
    """
    Validate the input slots to ensure only valid requests are being made
    :param slots:
    :return:
    """
    pass


# --- Intents ---

def dispatch_intent(intent_request):
    """
    Called when the user specifies an intent for this bot.
    """

    logger.debug(
        'dispatch userId={}, intentName={}'.format(intent_request['userId'], intent_request['currentIntent']['name']))

    intent_name = intent_request['currentIntent']['name']

    # Dispatch to your bot's intent handlers
    if intent_name == 'Hello':
        logger.info(intent_request)
        slots = intent_request['currentIntent']['slots']
        intent_name = slots['Services']
        slot_to_elicit = "Operations"
        return lex_response.elicit_slot(session_attributes={},
                                        intent_name=intent_name.title(),
                                        slots={},
                                        slot_to_elicit=slot_to_elicit)

    if intent_name == 'Instance':
        return monitor_instances(intent_request)

    raise Exception('Intent with name ' + intent_name + ' not supported')


# Main Handler
def lambda_handler(event, context):
    """
   Route the incoming request based on intent.
   The JSON body of the request is provided in the event slot.
   """

    logger.debug("Lambda Context:{}".format(context))
    logger.debug('event.bot.name={}'.format(event['bot']['name']))

    return dispatch_intent(event)
